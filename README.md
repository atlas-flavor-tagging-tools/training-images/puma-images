# puma images

This repository contains Docker images for the usage and development of the
[`puma` package](https://github.com/umami-hep/puma).
The images are built in the [`puma` repository on GitHub](https://github.com/umami-hep/puma) 
and then pushed to the container registry of this repository.

There is a slim image, which is meant for just using the latest version of `puma`.
You can start an interactive shell in a container with your current working directory 
mounted into the container by using one of the commands provided below.

On a machine with Docker installed:
```bash
docker run -it --rm -v $PWD:/puma_container -w /puma_container gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-images/puma-images/puma:latest bash
```
On a machine/cluster with singularity installed:
```bash
singularity shell --contain -B $PWD docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-images/puma-images/puma:latest
```

## Extended image for development

*For development, just replace the tag of the image*:

`latest` -> `latest-dev`

In addition to the minimal requirements that are required to use `puma`, the
`puma:latest-dev` image has the `requirements.txt` from the `puma` repo installed as
well.
This means that packages like `pytest`, `black`, `pylint`, etc. are installed as well.

**The images are automatically updated via GitHub and pushed to this [repository registry](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-images/puma-images/container_registry).**
